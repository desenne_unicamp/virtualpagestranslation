// author: Carolina de Senne Garcia
// RA 145656
// c145656@dac.unicamp.br
//
// author: Ricardo Henrique Pavan
// RA 176722
// r176722@dac.unicamp.br
//
// Description: https://lasca.ic.unicamp.br/paulo/cursos/so/2019s2/exp/exp02.html
//
// Compile this program:
// g++ traducao.cpp -o traducao
//
// Run this program:
// sudo ./traducao [pid]
// ATTENTION: you must run with root/admin privileges, otherwise pagemap will return
// all translations to physical addresses as address 0x00
//
// Warning: This code works in 64-bit machines, virtual pages of 4KB, RAM 2GB

#include<iostream>
#include<fstream>
#include <string>
#include <vector>
#include <map>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>

using namespace std;

// Machine RAM size in kB
uint64_t RAM_SIZE = 2048000;
// virtual pages size 4KB
uint64_t PAGE_SIZE = 4096;
// check if we want to print the translation (set true in case you want to see the translation)
bool printTraducao = true;
// check if we want to print the heat map (set true in case you want to print the HeatMap)
bool printHeatMap = false;

uint64_t hexstr2uint64_t(string s) {
	map<char, uint64_t> m;
	m['1']=1.0;m['2']=2.0;m['3']=3.0;m['4']=4.0;m['5']=5.0;
	m['6']=6.0;m['7']=7.0;m['8']=8.0;m['9']=9.0;m['a']=10.0;
	m['b']=11.0;m['c']=12.0;m['d']=13.0;m['e']=14.0;m['f']=15.0;
    uint64_t d = 0.0;
    for (int i=0; i < s.length(); i++) {
    	d += pow(16,s.length()-1-i)*m[s[i]];
    }
    return d;
}

// print mapping between RAM regions (end of region) and number of
// page frames mapped on those regions
void printRAMHeatMap(vector<uint64_t> pageFrames) {
	uint64_t regionSize = 32000; // size of region in kB
	int N = RAM_SIZE/regionSize; // number of regions
	uint64_t regionsCount[N]; // count of page frames on each region
	for (int i = 0; i < N; i++) {
		regionsCount[i] = 0;
	}
	// count page frames per region
	for (int i = 0; i < pageFrames.size(); i++) {
		int region = pageFrames[i]/regionSize;
		regionsCount[region]++;
	}
	for (int i = 0; i < N; i++) {
		printf("%ld\t%ld\n", (i+1)*regionSize, regionsCount[i]);
	}
}

int main (int argc, char **argv) {
	if (argc < 2) {
		cout << "Usage: ./traducao [pid]" << endl;
        exit(-1);
	}
	string pid = argv[1];
	// Access proc/[pid]/maps to find ranges of virtual addresses that belong to the process
    string mapsPath = "/proc/"+pid+"/maps";
    string line;
	ifstream mapsFile;
	mapsFile.open (mapsPath);
	if (!mapsFile) {
		cout << "Usage: ./traducao [pid]" << endl << "Please enter a valid pid!" << endl;
        exit(-1);
	}
	// ranges are stored linearly in the vector
	// pair indexes are starts of ranges, odd ones are ends. ex: [s1,e1,s2,e2...]
	vector<uint64_t> addrRanges;
	uint64_t limit = hexstr2uint64_t("ffffffffffff"); // limit on 48 bits
    while(!mapsFile.eof()) {
	    getline(mapsFile,line);
	    string range = line.substr(0,line.find(' '));
	    uint64_t start = hexstr2uint64_t(range.substr(0,range.find('-')));
	    uint64_t end = hexstr2uint64_t(range.substr(range.find('-')+1));
	    if (start <= limit || end <= limit) { // limit on 48 bits
	    	addrRanges.push_back(start);
	    	addrRanges.push_back(end);
		}
    }
	mapsFile.close();

	// Access proc/[pid]/pagemap to find virtual addresses (from the previous process ranges found)
	char pmPath[BUFSIZ];
	snprintf(pmPath, sizeof pmPath, "/proc/%d/pagemap", stoi(pid));
	int pmFile = open(pmPath, O_RDONLY);
    if(pmFile < 0) {
    	cout << "Error while opening pagemap file." << endl;
        exit(-1);
	}
	// store virtual pages not in RAM in notPresent vector
	vector<uint64_t> notPresent;
	// store virtual pages in RAM in present vector
	vector<uint64_t> present;
	// store page frames correspondent to virtual pages present in RAM (same order)
	vector<uint64_t> pageFrames;
	int i = 0;
	while (i < addrRanges.size()-1) {
		uint64_t start = addrRanges[i];
		uint64_t end = addrRanges[i+1];
		for(uint64_t addr = start; addr < end; addr += PAGE_SIZE) {
			// read line in pagemap (each line has 64 bits = 8 bytes)
			uint64_t line;
			uint64_t index = (addr/PAGE_SIZE) * sizeof(line);
			if(pread(pmFile, &line, sizeof(line), index) != sizeof(line)) {
            	cout << "Error while reading pagemap file." << endl;
            	exit(-1);
        	}
        	// if present in RAM (bit 63 set), insert in map virtual page to page frame (bits 0-54)
        	if ((line >> 63) & 1) {
        		uint64_t pfn = (line & 0x7fffffffffffff);
        		present.push_back(addr);
        		pageFrames.push_back(pfn);
        	} else { // else, insert in notPresent vector
        		notPresent.push_back(addr);
        	}			
		}
		i+=2;
	}

	// Print heat map
	if (printHeatMap) {
		printRAMHeatMap(pageFrames);
	}
	// Print translation
	if (printTraducao) {
		cout << "Páginas virtuais não mapeadas (não presentes em RAM)" << endl;
		for (int i = 0; i < notPresent.size(); i++) {
			printf("0x%-12lx\n", notPresent[i]);
		}
		cout << "Páginas virtuais mapeadas (presentes em RAM) : Page frames correspondentes" << endl;
		for (int i = 0; i < present.size(); i++) {
			printf("0x%-12lx : 0x%-14lx\n", notPresent[i], pageFrames[i]);
		}
	}
	return 0;
}
